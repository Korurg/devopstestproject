package korurg.devopstestproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevOpsTestProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(DevOpsTestProjectApplication.class, args);
    }

}