package korurg.devopstestproject.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @Value("${build.version}")
    private String buildVersion;

    @GetMapping("/hello")
    public String getHelloWorld() {
        return "Hello world " + buildVersion;
    }
}