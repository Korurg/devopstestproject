FROM amazoncorretto:17-alpine

WORKDIR /app
COPY ./DevOpsTestProject.jar .

ENTRYPOINT ["java", "-jar", "./DevOpsTestProject.jar"]